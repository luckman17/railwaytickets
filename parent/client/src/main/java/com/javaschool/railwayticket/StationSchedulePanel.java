package com.javaschool.railwayticket;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import com.javaschool.railwayticket.FullSchedulePanel.ApplyFilterActionListener;

public class StationSchedulePanel extends JPanel {
	private static final long serialVersionUID = -3793086803044963334L;
	private static final int BUTTONS_NUM = 4;	
	private JComboBox stationFilter;
	
	private JTable stationScheduleTable;
	private TableModelStationSchedule tableModel;
	
	StationSchedulePanel() {
		super(new BorderLayout());
		this.createPanel();
	}
	private void createPanel() {
		this.addTablePanel();
		this.addButtonPanel();
		this.addFilterPanel();
		stationFilter.setSelectedIndex(0);
	}
	
	private void addFilterPanel() {
		JPanel filterPanel = new JPanel(new FlowLayout());
		
		JLabel stationLabel = new JLabel("Station: ");
		filterPanel.add(stationLabel);
		List<Station> stationList = ClientRequest.getAllStations();
		stationFilter = new JComboBox(stationList.toArray());		
		stationFilter.addActionListener(new StationFilterActionListener());
		stationLabel.setLabelFor(stationFilter);
		filterPanel.add(stationFilter);
		
		this.add(filterPanel, BorderLayout.NORTH);
	}
	
	private void addButtonPanel() {
		GridLayout gridLayout = new GridLayout(BUTTONS_NUM, 1);
		gridLayout.setVgap(5);
		JPanel buttonPanel = new JPanel(gridLayout);
		
		JButton viewLinesButton = new JButton("View route stations");
		viewLinesButton.addActionListener(new ViewRouteStationsActionListener(stationScheduleTable));
		buttonPanel.add(viewLinesButton);		
		
		JButton buyTicketButton = new JButton("Buy ticket");
		buyTicketButton.addActionListener(new AddPassengerActionListener(stationScheduleTable, this));
		buttonPanel.add(buyTicketButton);
		
		JPanel east = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.weighty = 1.0;
		east.add(buttonPanel);
		
		this.add(east, BorderLayout.EAST);		
	}
	
	private void addTablePanel() {
		JPanel tablePanel = new JPanel(new GridLayout(1, 1));
		
		this.initTable();
		
		JScrollPane jScrollPane = new JScrollPane(stationScheduleTable);
		tablePanel.add(jScrollPane);

		this.add(tablePanel, BorderLayout.CENTER);
	}
	
	private void initTable() {
		tableModel = new TableModelStationSchedule(ClientRequest.getFullSchedule(), null); 
		stationScheduleTable = new JTable(tableModel);
		stationScheduleTable.setFillsViewportHeight(true);
		stationScheduleTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		stationScheduleTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	class StationFilterActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			ScheduleFilter scheduleFilter = new ScheduleFilter();
			scheduleFilter.dateStart = null;
			scheduleFilter.stationIdTo = 0;
			Station station = (Station)stationFilter.getSelectedItem();
			scheduleFilter.stationIdFrom = station.getId();
			stationScheduleTable.setModel(new TableModelStationSchedule(
					ClientRequest.getFilteredSchedule(scheduleFilter),
					ClientRequest.getStationById(scheduleFilter.stationIdFrom)));
		}		
	}
}
