package com.javaschool.railwayticket;

import java.io.*;
import java.net.Socket;

public class SimpleClient {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		try (Socket client = new Socket("localhost", 8080)) {
			ObjectOutputStream output = new ObjectOutputStream(new BufferedOutputStream(client.getOutputStream()));
			output.writeObject(new Request("Get full schedule"));
			output.flush();
			client.shutdownOutput();
			
			ObjectInputStream input = new ObjectInputStream(client.getInputStream());
			
			Object obj = input.readObject();
			System.out.println(obj.toString());	
			System.out.println();
		}
	}

}
