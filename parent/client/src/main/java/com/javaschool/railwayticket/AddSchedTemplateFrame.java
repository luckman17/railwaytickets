package com.javaschool.railwayticket;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

public class AddSchedTemplateFrame extends JFrame {
	private final Logger logger = Logger.getLogger(this.getClass());
	private static final long serialVersionUID = -3261558173101504126L;
	private TableModelSchedTemplateLines tableModel;
	private SchedTemplate schedTemplate = new SchedTemplate();
	private JComboBox stationBox;
	private JTextField timeText;
	private JTextField stNameText;
	
	AddSchedTemplateFrame() {
		super("Add Route");
		
		JPanel schedTemplatePanel = new JPanel(new FlowLayout());
		JLabel stNameLabel = new JLabel("Name: ");
		stNameText = new JTextField(20);
		stNameLabel.setLabelFor(stNameText);
		schedTemplatePanel.add(stNameLabel);
		schedTemplatePanel.add(stNameText);		
		add(schedTemplatePanel, BorderLayout.NORTH);
		
		JPanel tablePanel = new JPanel(new GridLayout(1, 1));
		tableModel = new TableModelSchedTemplateLines(
				new ArrayList<SchedTemplateLines>());
		JTable stlTable = new JTable(tableModel);
		JScrollPane scrollPane = new JScrollPane(stlTable);
		tablePanel.add(scrollPane);
		add(tablePanel, BorderLayout.CENTER);
		
		JPanel stationPanel = new JPanel();
		stationPanel.setLayout(new GridLayout(10, 1));
		List<Station> stations = ClientRequest.getAllStations();
		stationBox = new JComboBox(stations.toArray());
		stationPanel.add(stationBox);
		JLabel timeLabel = new JLabel("Time: ");
		timeText = new JTextField(1);
		timeLabel.setLabelFor(timeText);
		stationPanel.add(timeLabel);
		stationPanel.add(timeText);
		JButton addStationButton = new JButton("Add");
		addStationButton.addActionListener(new AddButtonActionListener());
		stationPanel.add(addStationButton);
		add(stationPanel, BorderLayout.EAST);
		
		JPanel okCancelPanel = new JPanel(new FlowLayout());
		JButton okButton = new JButton("Ok");
		JButton cancelButton = new JButton("Cancel");
		okButton.addActionListener(new OkButtonActionListener());
		cancelButton.addActionListener(new CancelButtonActionListener());
		okCancelPanel.add(okButton);
		okCancelPanel.add(cancelButton);
		
		add(okCancelPanel, BorderLayout.SOUTH);
		
		pack();
		setVisible(true);
	}
	
	class AddButtonActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			int num = tableModel.getRowCount();
			SchedTemplateLines stl = new SchedTemplateLines();
			stl.setKey(new SchedTemplateLinesPK());
			stl.getKey().setPointNum(num + 1);
			Station station = (Station)stationBox.getSelectedItem(); 
			stl.setStation(station);
			try {
				stl.setTimePass(Global.timeFormat.parse(timeText.getText()));
			}
			catch (ParseException exception) {
				JOptionPane.showMessageDialog(null, "Wrong time format");
				return;
			}
			String validationString = ValidationHandler.getValidationErrors(stl);
			if (!validationString.equals("")) {
				JOptionPane.showMessageDialog(null, validationString);
				return;
			}
			tableModel.addScheduleTemplateLine(stl);
			tableModel.fireTableDataChanged();
			timeText.setText("");
		}
	}
	
	class OkButtonActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			schedTemplate.setTemplateName(stNameText.getText());
			schedTemplate.setSchedTemplateLines(tableModel.getScheduleTemplateList());
			String validationString = ValidationHandler.getValidationErrors(schedTemplate);
			if (!validationString.equals("")) {
				JOptionPane.showMessageDialog(null, validationString);
				return;
			}			
			String result = ClientRequest.addSchedTemplate(schedTemplate);
			JOptionPane.showMessageDialog(null, result);
			AddSchedTemplateFrame.this.setVisible(false);
		}
	}
	
	class CancelButtonActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			AddSchedTemplateFrame.this.setVisible(false);
		}		
	}
}
