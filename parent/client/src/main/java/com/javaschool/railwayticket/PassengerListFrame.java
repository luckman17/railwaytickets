package com.javaschool.railwayticket;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class PassengerListFrame extends JFrame {
	private static final long serialVersionUID = 3301524321500668722L;
	Schedule schedule;
	
	public PassengerListFrame(Schedule s) {
		super("Passenger List");
		this.schedule = s;
		
		List<Passenger> passengerList = ClientRequest.getPassengerList(schedule);
		
		JTable passengerTable = new JTable(
				new TableModelPassenger(passengerList));
		add(new JScrollPane(passengerTable));
		pack();
		setVisible(true);
	}
}
