package com.javaschool.railwayticket;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.validation.ConstraintViolation;

import org.apache.log4j.Logger;

public class AddPassengerActionListener implements ActionListener {
	private final Logger logger = Logger.getLogger(this.getClass()); 
	private JTable table;
	Component parent;
	public AddPassengerActionListener(JTable table, Component parentComponent) {
		this.table = table;
		this.parent = parentComponent;
	}
	public void actionPerformed(ActionEvent e) {
		if (table.getSelectedRow() < 0) {
			JOptionPane.showMessageDialog(null, "You should choose schedule in table");
			logger.warn("Add passenger - not choosed schedule row");
			return;
		}
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		JTextField firstName = new JTextField(15);
		JTextField lastName = new JTextField(15);
		JTextField birthdate = new JTextField(15);
		
		Passenger passenger = new Passenger();
		
		panel.add(new JLabel("First name: "));
		panel.add(firstName);
		panel.add(Box.createVerticalStrut(10));
		panel.add(new JLabel("Last name: "));
		panel.add(lastName);
		panel.add(Box.createVerticalStrut(10));
		panel.add(new JLabel("Birthdate"));
		panel.add(birthdate);
		
		int result = JOptionPane.showConfirmDialog(null, panel,
				"Enter personal information", JOptionPane.OK_CANCEL_OPTION);
		if (result == JOptionPane.OK_OPTION) {			
			try {
				passenger.setBirthdate(Global.dateFormat.parse(birthdate.getText()));
			}
			catch (ParseException exception) {
				JOptionPane.showInternalMessageDialog(null, "Wrong date format!");
				logger.warn("Wrong date format");
				return;
			}
			passenger.setFirstname(firstName.getText());
			passenger.setLastname(lastName.getText());
			
			String validationString = ValidationHandler.getValidationErrors(passenger);
			if (!validationString.equals("")) {
				JOptionPane.showMessageDialog(parent, validationString);
				logger.warn(validationString);
				return;
			}
				
			int selectedRow = table.getSelectedRow();
			long scheduleId = (Long)table.getValueAt(selectedRow, 0);
			Response response = ClientRequest.buyTicket(
					scheduleId, passenger);
			if (response.error) {
				if (!response.message.equals("")) {
					JOptionPane.showMessageDialog(parent, "Error: " + response.message);
					logger.warn(response.message);
				}
				else {
					if (response.obj != null && response.obj instanceof Throwable) {
						logger.error("Response Error: ", (Throwable)response.obj);
					}
				}
			}
			else {
				JOptionPane.showMessageDialog(parent, response.message);
			}
		}
	}
}
