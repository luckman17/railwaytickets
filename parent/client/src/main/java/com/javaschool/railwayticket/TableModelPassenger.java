package com.javaschool.railwayticket;

import java.util.List;

import javax.swing.table.AbstractTableModel;


public class TableModelPassenger extends AbstractTableModel {
	private static final long serialVersionUID = 6034166003477787626L;
	enum PassengerColumn {
		ID, FIRSTNAME, LASTNAME, BIRTHDATE
	}
	private final String[] columnNames = {"Id", "Firstname", "Lastname",
			"Birthdate"};
	private final int COLUMN_COUNT = 4;
	
	List<Passenger> passengerList;
	
	public TableModelPassenger(List<Passenger> passengerList) {
		this.passengerList = passengerList;
	}

	@Override
	public int getColumnCount() {
		return COLUMN_COUNT;
	}

	@Override
	public int getRowCount() {
		return passengerList.size();
	}
	
	@Override
	public Class<?> getColumnClass(int c) {
		return String.class;
	}	

	@Override
	public Object getValueAt(int r, int c) {
		PassengerColumn pc = (PassengerColumn.values())[c];
		Passenger passenger = passengerList.get(r);
		switch (pc) {
			case ID: return passenger.getId();
			case FIRSTNAME: return passenger.getFirstname();
			case LASTNAME: return passenger.getLastname();
			case BIRTHDATE: return Global.dateFormat.format(passenger.getBirthdate());
			default: return null;
		}
	}
	@Override
	public String getColumnName(int c) {
		return columnNames[c];
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}	
	
}
