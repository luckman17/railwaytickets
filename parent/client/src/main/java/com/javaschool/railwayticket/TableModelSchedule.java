package com.javaschool.railwayticket;

import java.util.List;

import javax.swing.table.AbstractTableModel;

public abstract class TableModelSchedule extends AbstractTableModel {
	private static final long serialVersionUID = -10393453877037084L;
	protected List<Schedule> schedList;

	public int getRowCount() {
		return schedList.size();
	}
	public Schedule getSchedule(int r) {
		return schedList.get(r);
	}
	public void addSchedule(Schedule schedule) {
		schedList.add(schedule);
	}
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	
}
