package com.javaschool.railwayticket;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class ClientMainFrame extends JFrame {
	private static final long serialVersionUID = 2635310098401181001L;
	private JPanel fullSchedulePanel;
	
	public ClientMainFrame() {
		super("Railway tickets client");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		fullSchedulePanel = new FullSchedulePanel();

		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.addTab("Schedule", fullSchedulePanel);
		tabbedPane.addTab("Station Schedule", new StationSchedulePanel());
		
		this.add(tabbedPane, BorderLayout.CENTER);
		pack();
		setVisible(true);
	}
	
	
	public static void main(String[] args) {
		new ClientMainFrame();
	}
	
	
}

