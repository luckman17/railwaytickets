package com.javaschool.railwayticket;

import java.util.List;

import javax.swing.table.AbstractTableModel;

public class TableModelFullSchedule extends TableModelSchedule {
	private static final long serialVersionUID = -1615504088333147748L;
	
	enum ScheduleColumn {
		ID, TEMPLATE_NAME, TRAIN_NUM,
		EMPTY_SEATS, STATION_FROM, STATION_TO,
		DATE_START, TIME_START
	}
	private final String[] columnNames = {"Id", "Route Name", "Train#",
			"Empty seats", "Departure Station", "Arrival Station",
			"Departure Date", "Departure Time"};
	
	public TableModelFullSchedule(List<Schedule> scheduleList) {
		super();
		schedList = scheduleList;
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int r, int c) {
		ScheduleColumn sc = (ScheduleColumn.values())[c];
		Schedule schedule = schedList.get(r);
		List<SchedTemplateLines> templateLines = schedule.getSchedTemplate().getSchedTemplateLines();
		switch (sc) {
		case ID: return schedule.getId();
		case TEMPLATE_NAME: return schedule.getSchedTemplate().getTemplateName();
		case TRAIN_NUM: return schedule.getTrain().getNumber();
		case EMPTY_SEATS: return schedule.getEmptySeats();
		case STATION_FROM: {
			return templateLines.get(0).getStation().getName();
		}
		case STATION_TO: {
			return templateLines.get(templateLines.size() - 1).getStation().getName();
		}
		case DATE_START: return Global.dateFormat.format(schedule.getDateStart());
		case TIME_START: return templateLines.get(0).getTimePass();
		}
		return null;
	}
	
	@Override
	public String getColumnName(int c) {
		return columnNames[c];
	}

	@Override
	public Class<?> getColumnClass(int c) {
		return String.class;
	}
	
}
