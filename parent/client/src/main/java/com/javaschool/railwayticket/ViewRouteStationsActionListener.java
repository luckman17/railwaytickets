package com.javaschool.railwayticket;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

class ViewRouteStationsActionListener implements ActionListener {
	JTable scheduleTable;
	TableModelSchedule tableModelSchedule;
	
	public ViewRouteStationsActionListener(JTable scheduleTable) {
		this.scheduleTable = scheduleTable;
		this.tableModelSchedule = (TableModelSchedule)scheduleTable.getModel();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (scheduleTable.getSelectedRow() < 0) {
			JOptionPane.showMessageDialog(null, "You should choose schedule in table");
			return;
		}		
		JPanel panel = new JPanel();
		Schedule schedule = tableModelSchedule.getSchedule(scheduleTable.getSelectedRow());
		List<SchedTemplateLines> stlList = schedule.getSchedTemplate().getSchedTemplateLines();
		JTable stlTable = new JTable(new TableModelSchedTemplateLines(stlList));
		JScrollPane scrollPane = new JScrollPane(stlTable);
		panel.add(scrollPane);
		JFrame stlFrame = new JFrame("Route stations");
		stlFrame.add(panel);
		stlFrame.pack();
		stlFrame.setVisible(true);
	}		
}
