package com.javaschool.railwayticket;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.javaschool.railwayticket.TableModelPassenger.PassengerColumn;

public class TableModelTrain extends AbstractTableModel {	
	enum TrainColumn {
		NUM, SEATS
	}
	private final String[] columnNames = {"Train number", "Seats"};
	
	List<Train> trainList;
	
	public TableModelTrain(List<Train> trainList) {
		this.trainList = trainList;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return trainList.size();				
	}
	
	@Override
	public Class<?> getColumnClass(int c) {
		return String.class;
	}	

	@Override
	public Object getValueAt(int r, int c) {
		TrainColumn tc = (TrainColumn.values())[c];
		Train train = trainList.get(r);
		switch (tc) {
			case NUM: return train.getNumber();
			case SEATS: return train.getSeats();
			default: return null;
		}
	}
	@Override
	public String getColumnName(int c) {
		return columnNames[c];
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	

}
