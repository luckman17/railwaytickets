package com.javaschool.railwayticket;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

public class ClientRequest {
	private static final Logger logger = Logger.getLogger(ClientRequest.class);
	
	private ClientRequest() {}
	
	@SuppressWarnings("unchecked")
	static public List<Schedule> getFullSchedule() {
		Response response = new ClientRequest().getResponse(new Request("Get full schedule"));
		List<Schedule> ret = null;
		if (!response.error)
			ret = (List<Schedule>)response.obj;
		else {
			new ClientRequest().showErrorMessage(response);
		}
		return ret;
	}	

	@SuppressWarnings("unchecked")
	static public List<Schedule> getFilteredSchedule(ScheduleFilter scheduleFilter) {
		Response response = new ClientRequest().getResponse(
				new Request("Get filtered schedule", scheduleFilter));
		List<Schedule> ret = null;
		if (!response.error)
			ret = (List<Schedule>)response.obj;
		else {
			new ClientRequest().showErrorMessage(response);
		}
		return ret;
	}
	@SuppressWarnings("unchecked")
	static public List<Passenger> getPassengerList(Schedule schedule) {
		Response response = new ClientRequest().getResponse(
				new Request("Get schedule passenger list", schedule));
		List<Passenger> ret = null;
		if (!response.error) {
			ret = (List<Passenger>)response.obj;			
		}
		else {
			new ClientRequest().showErrorMessage(response);
		}
		return ret;
	}
	public static Station getStationById(long id) {
		Response response = new ClientRequest().getResponse(
				new Request("Get station by id", id));
		Station ret = null;
		if (!response.error)
			ret = (Station)response.obj;
		else {
			new ClientRequest().showErrorMessage(response);
		}
		return ret;
	}
	public static Response buyTicket(long scheduleId, Passenger passenger) {
		return new ClientRequest().getResponse(
				new Request("Buy ticket", new Tuple<Long, Passenger>(scheduleId, passenger)));
	}
	
	public static String addTrain(Train t) {
		Response response = new ClientRequest().getResponse(new Request("Add train", t));
		String ret = "Train added!";
		if (response.error)
			ret = response.message;
		return ret;
	}
	
	public static String addStation(Station s) {
		Response response = new ClientRequest().getResponse(new Request("Add station", s));
		String ret = "Station added!";
		if (response.error)
			ret = response.message;
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Station> getAllStations() {
		Response response = new ClientRequest().getResponse(new Request("Get all stations"));
		List<Station> ret = null;
		if (!response.error) {
			ret = (List<Station>)response.obj;
		}
		else {
			new ClientRequest().showErrorMessage(response);
		}
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	public static List<SchedTemplate> getAllSchedTemplates() {
		Response response = new ClientRequest().getResponse(new Request("Get all schedTemplates"));
		List<SchedTemplate> ret = null;
		if (!response.error) {
			ret = (List<SchedTemplate>)response.obj;
		}
		else {
			new ClientRequest().showErrorMessage(response);
		}
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Train> getAllTrains() {
		Response response = new ClientRequest().getResponse(new Request("Get all trains"));
		List<Train> ret = null;
		if (!response.error) {
			ret = (List<Train>)response.obj;
		}
		else {
			new ClientRequest().showErrorMessage(response);
		}
		return ret;
	}
	
	public static String addSchedTemplate(SchedTemplate schedTemplate) {
		Response response = new ClientRequest().getResponse(new Request("Add schedTemplate", schedTemplate));
		String ret = "Route added!";
		if (response.error) {
			ret = response.message;
		}
		return ret;				
	}
	
	public static String addSchedule(Schedule schedule) {
		Response response = new ClientRequest().getResponse(new Request("Add schedule", schedule));
		String ret = "Schedule added!";
		if (response.error) {
			ret = response.message;
		}
		return ret;
	}
	
	public static boolean login(User u) {
		Response response = new ClientRequest().getResponse(new Request("Login", u));
		return !response.error;
	}
	
	public static int getTicketsLeft(Schedule s) {
		Response response = new ClientRequest().getResponse(new Request("Get tickets left", s));
		int ret;
		if (!response.error) {
			ret = (Integer)response.obj;
		}
		else {
			ret = 0;
			new ClientRequest().showErrorMessage(response);
		}
		return ret;
	}
	
	private Response getResponse(Request request) {
		Response ret = null;
		try (Socket client = new Socket("localhost", 8080)) {
			ObjectOutputStream output = new ObjectOutputStream(
					new BufferedOutputStream(client.getOutputStream()));
			output.writeObject(request);
			output.flush();
			client.shutdownOutput();
			
			ObjectInputStream input = new ObjectInputStream(client.getInputStream());
		
			ret = (Response)input.readObject();			
		}
		catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Cannot connect to Server!");
			logger.error("Connection could not be established");
		}
		catch (ClassNotFoundException e) {
			logger.fatal("Unexpected response");
		}
		return ret;		
	}
	
	private void showErrorMessage(Response response) {
		if (!response.message.equals("")) {
			JOptionPane.showMessageDialog(null, "Server error: " + response.message);
			logger.error("Server error: " + response.message);
		}
		else {
			JOptionPane.showMessageDialog(null, "Server error!");
			if (response.obj instanceof Throwable) {
				logger.error("Server error!", (Throwable)response.obj);
			}
			else {	// impossible?
				logger.fatal("Unexpected error on server" + response);
			}
		}		
	}

}
