package com.javaschool.railwayticket;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class TrainListFrame extends JFrame {
	private static final long serialVersionUID = -1195044857787929402L;

	public TrainListFrame() {
		super("Train List");
		
		List<Train> trainList = ClientRequest.getAllTrains();
		
		JTable trainTable = new JTable(
				new TableModelTrain(trainList));
		add(new JScrollPane(trainTable));
		pack();
		setVisible(true);
		
	}
}
