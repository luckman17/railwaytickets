package com.javaschool.railwayticket;

import java.util.List;

import javax.swing.table.AbstractTableModel;

public class TableModelSchedTemplateLines extends AbstractTableModel {
	private final String[] columnNames = {"Point#", "Station", "Time pass"};
	private List<SchedTemplateLines> stlList;
	
	TableModelSchedTemplateLines(List<SchedTemplateLines> stlList) {
		this.stlList = stlList;
	}
	
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return stlList.size();
	}

	@Override
	public Object getValueAt(int r, int c) {
		SchedTemplateLines line = stlList.get(r);
		switch (c) {
			case 0: return line.getKey().getPointNum();
			case 1: return line.getStation().toString();
			case 2: return Global.timeFormat.format(line.getTimePass());
			default: return 0;
		}
	}

	
	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	
	public void addScheduleTemplateLine(SchedTemplateLines stl) {
		stlList.add(stl);
	}
	
	public List<SchedTemplateLines> getScheduleTemplateList() {
		return stlList;
	}
}
