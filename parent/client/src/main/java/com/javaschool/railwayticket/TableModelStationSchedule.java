package com.javaschool.railwayticket;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import javax.swing.table.AbstractTableModel;

public class TableModelStationSchedule extends TableModelSchedule {
	enum StationScheduleColumn {
		ID, TEMPLATE_NAME, TRAIN_NUM, EMPTY_SEATS, STATION_TO,
		DATETIME_PASS, DATETIME_ARRIVAL
	}
	private final String[] columnNames = {"Id", "Route Name", "Train#", "Empty seats", 
			"Arrival Station", "Passage date and time", "Arrival date and time"};
	
	private Station station;
	
	public TableModelStationSchedule(List<Schedule> scheduleList, Station station) {
		super();
		this.schedList = scheduleList;
		this.station = station;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return schedList.size();
	}

	@Override
	public Object getValueAt(int r, int c) {
		StationScheduleColumn sc = (StationScheduleColumn.values())[c];
		Schedule schedule = schedList.get(r);
		List<SchedTemplateLines> templateLines = schedule.getSchedTemplate().getSchedTemplateLines();
		switch (sc) {
			case ID: return schedule.getId();
			case TEMPLATE_NAME: return schedule.getSchedTemplate().getTemplateName();
			case TRAIN_NUM: return schedule.getTrain().getNumber();
			case EMPTY_SEATS: return schedule.getEmptySeats();
			case STATION_TO: return templateLines.get(templateLines.size() - 1).getStation().getName();
			case DATETIME_PASS:
				return station != null ? Global.dateTimeFormat.format(schedule.getStationDateTime(station)) : "";
			case DATETIME_ARRIVAL: return Global.dateTimeFormat.format(schedule.getFinalStationDateTime());
			default: return null;
		}
	}
	@Override
	public String getColumnName(int c) {
		return columnNames[c];
	}

	@Override
	public Class<?> getColumnClass(int c) {
		return String.class;
	}

}
