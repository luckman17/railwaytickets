package com.javaschool.railwayticket;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

public class FullSchedulePanel extends JPanel {
	private final Logger logger = Logger.getLogger(this.getClass());
	private static final long serialVersionUID = -8681741258475693333L;
	private static final int BUTTONS_NUM = 12;	
	private JTextField dateFilter;
//	private JTextField stationFromFilter;
//	private JTextField stationToFilter;
	JComboBox stationFromFilter;
	JComboBox stationToFilter;
	private JButton applyFilterButton;
	private JButton buyTicketButton;
	private JButton addTrainButton;
	private JButton addStationButton;
	private JButton addScheduleTemplateButton;
	private JButton addScheduleButton;
	private JButton viewTrainsButton;
	private JButton viewPassengersButton;
	private JButton loginButton;
	List<Station> stationList;
	
	private JTable scheduleTable;
	private TableModelSchedule tableModelSchedule;	
	
	FullSchedulePanel() {
		super(new BorderLayout());
		this.createPanel();
	}
	private void createPanel() {
		this.addTablePanel();
		this.addButtonPanel();
		this.addFilterPanel();
	}
	
	private void addFilterPanel() {
		JPanel filterPanel = new JPanel(new FlowLayout());
		
		JLabel dateLabel = new JLabel("Date: ");
		filterPanel.add(dateLabel);
		dateFilter = new JTextField();
		dateLabel.setLabelFor(dateFilter);
		dateFilter.setPreferredSize(new Dimension(70, 25));
		filterPanel.add(dateFilter);
		
		JLabel stationFromLabel = new JLabel("From: ");
		filterPanel.add(stationFromLabel);
		stationList = ClientRequest.getAllStations();
		stationList.add(0, new Station());
		stationFromFilter = new JComboBox(stationList.toArray());
		stationFromLabel.setLabelFor(stationFromFilter);
		filterPanel.add(stationFromFilter);
		
		JLabel stationToLabel = new JLabel("To: ");//, SwingConstants.TRAILING);
		filterPanel.add(stationToLabel);
		stationToFilter = new JComboBox(stationList.toArray());
		stationToLabel.setLabelFor(stationToFilter);
		filterPanel.add(stationToFilter);
		
		this.add(filterPanel, BorderLayout.NORTH);
	}
	
	private void addButtonPanel() {
		GridLayout gridLayout = new GridLayout(BUTTONS_NUM, 1);
		gridLayout.setVgap(5);
		JPanel buttonPanel = new JPanel(gridLayout);
		
		applyFilterButton = new JButton("Apply filters");
		buttonPanel.add(applyFilterButton);
		applyFilterButton.addActionListener(new ApplyFilterActionListener());
		
		JButton viewLinesButton = new JButton("View route stations");
		viewLinesButton.addActionListener(new ViewRouteStationsActionListener(scheduleTable));
		buttonPanel.add(viewLinesButton);		
		
		buyTicketButton = new JButton("Buy ticket");
		buyTicketButton.addActionListener(new AddPassengerActionListener(scheduleTable, this));
		buttonPanel.add(buyTicketButton);	
		
		addTrainButton = new JButton("Add train");
		addTrainButton.addActionListener(new AddTrainActionListener());
		buttonPanel.add(addTrainButton);
		
		addStationButton = new JButton("Add station");
		addStationButton.addActionListener(new AddStationActionListener());
		buttonPanel.add(addStationButton);
		
		viewTrainsButton = new JButton("View all trains");
		viewTrainsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new TrainListFrame();
			}
		});
		buttonPanel.add(viewTrainsButton);
		
		viewPassengersButton = new JButton("View passenger list");
		viewPassengersButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Schedule schedule = ((TableModelSchedule)scheduleTable.getModel()).getSchedule(scheduleTable.getSelectedRow());
				new PassengerListFrame(schedule);
			}
		});
		buttonPanel.add(viewPassengersButton);
		
		addScheduleTemplateButton = new JButton("Add Route");
		addScheduleTemplateButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new AddSchedTemplateFrame();
			}
		});
		buttonPanel.add(addScheduleTemplateButton);
		
		addScheduleButton = new JButton("Add schedule");
		addScheduleButton.addActionListener(new AddScheduleActionListener());
		buttonPanel.add(addScheduleButton);
		
		loginButton = new JButton("Login");
		loginButton.addActionListener(new LoginActionListener());
		buttonPanel.add(loginButton);
		
		JPanel east = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.weighty = 1.0;
		setUserButtonsVisible(false);
		east.add(buttonPanel);
		
		this.add(east, BorderLayout.EAST);		
	}
	
	private void addTablePanel() {
		JPanel tablePanel = new JPanel(new GridLayout(1, 1));
		
		this.initTable();
		
		JScrollPane jScrollPane = new JScrollPane(scheduleTable);
		tablePanel.add(jScrollPane);

		this.add(tablePanel, BorderLayout.CENTER);
	}
	
	private void initTable() {
		tableModelSchedule = new TableModelFullSchedule(ClientRequest.getFullSchedule()); 
		scheduleTable = new JTable(tableModelSchedule);
		scheduleTable.setFillsViewportHeight(true);
		scheduleTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		scheduleTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}
	
	private void setUserButtonsVisible(boolean visible) {
		addTrainButton.setVisible(visible);
		addStationButton.setVisible(visible);
		addScheduleTemplateButton.setVisible(visible);
		addScheduleButton.setVisible(visible);
		viewTrainsButton.setVisible(visible);
		viewPassengersButton.setVisible(visible);
		
		// not visible if logged in
		loginButton.setVisible(!visible);
	}
	
	class ApplyFilterActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			ScheduleFilter scheduleFilter = new ScheduleFilter();
			if (dateFilter.getText().equals("")) {
				scheduleFilter.dateStart = null;
			}
			else {
				try {
					scheduleFilter.dateStart = Global.dateFormat.parse(dateFilter.getText());
				}
				catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Wrong date");
					return;
				}
			}
			Station stationFrom, stationTo;
			stationFrom = (Station)stationFromFilter.getSelectedItem();
			stationTo = (Station)stationToFilter.getSelectedItem();
			scheduleFilter.stationIdFrom = stationFrom.getId();
			scheduleFilter.stationIdTo = stationTo.getId();
			scheduleTable.setModel(new TableModelFullSchedule(ClientRequest.getFilteredSchedule(scheduleFilter)));
			logger.info("Apply filter: " + scheduleFilter);			
		}	
	}
	
	class AddTrainActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JPanel panel = new JPanel();
			JTextField seats = new JTextField(3);
			
			panel.add(new JLabel("Seats: "));
			panel.add(seats);
			
			int result = JOptionPane.showConfirmDialog(null, panel,
					"Enter train information", JOptionPane.OK_CANCEL_OPTION);
			if (result == JOptionPane.OK_OPTION) {
				Train t = new Train();
				int seatsNum;
				try {
					seatsNum = Integer.parseInt(seats.getText());
				} catch (Exception e) {
					// TODO Error
					return;
				}
				t.setSeats(seatsNum);
				
				String message = ClientRequest.addTrain(t);
				JOptionPane.showMessageDialog(null, message);
				logger.info("Added train: " + t);
			}
		}	
	}
	
	class AddStationActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JPanel panel = new JPanel();
			JTextField name = new JTextField(10);
			
			panel.add(new JLabel("Station name: "));
			panel.add(name);
			
			int result = JOptionPane.showConfirmDialog(null, panel,
					"Enter station information", JOptionPane.OK_CANCEL_OPTION);
			if (result == JOptionPane.OK_OPTION) {
				Station s = new Station();
				s.setName(name.getText());
				String message = ClientRequest.addStation(s);
				JOptionPane.showMessageDialog(null, message);
				logger.info("Added station: " + s);
			}			
		}		
	}
	
	class AddScheduleActionListener implements ActionListener {
		@SuppressWarnings("rawtypes")
		@Override
		public void actionPerformed(ActionEvent e) {
			JPanel panel = new JPanel();
			List<SchedTemplate> listSchedTemplate = ClientRequest.getAllSchedTemplates();
			JComboBox comboBoxTemplate =
					new JComboBox(listSchedTemplate.toArray());
			panel.add(comboBoxTemplate);
			List<Train> listTrain = ClientRequest.getAllTrains();
			JComboBox comboBoxTrain = 
					new JComboBox(listTrain.toArray());
			panel.add(comboBoxTrain);
			panel.add(new JLabel("Departure date:"));
			JTextField dateRelease = new JTextField(10);
			panel.add(dateRelease);
			
			int result = JOptionPane.showConfirmDialog(null, panel,
					"Add schedule information", JOptionPane.OK_CANCEL_OPTION);
			if (result == JOptionPane.OK_OPTION) {
				Schedule schedule = new Schedule();
				SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
				try {
					schedule.setDateStart(sdf.parse(dateRelease.getText()));					
				}
				catch (ParseException exception) {
					JOptionPane.showMessageDialog(null, "Wrong date format");
					return;
				}
				schedule.setSchedTemplate((SchedTemplate)comboBoxTemplate.getSelectedItem());
				schedule.setTrain((Train)comboBoxTrain.getSelectedItem());
				
				String validationString = ValidationHandler.getValidationErrors(schedule);
				if (!validationString.equals("")) {
					JOptionPane.showMessageDialog(null, validationString);
					return;
				}				
				final String message = ClientRequest.addSchedule(schedule);
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						JOptionPane.showMessageDialog(null, message);						
					}
				});
				new ApplyFilterActionListener().actionPerformed(e);
				logger.info("Added schedule: " + schedule.getId());
			}
		}		
	}
	
	class LoginActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JPanel panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			JTextField loginText = new JTextField(10);
			JTextField passwordText = new JTextField(10);
			panel.add(new JLabel("Login"));
			panel.add(loginText);
			panel.add(new JLabel("Password"));
			panel.add(passwordText);
			
			int result = JOptionPane.showConfirmDialog(null, panel,
					"Enter user information", JOptionPane.OK_CANCEL_OPTION);
			if (result == JOptionPane.OK_OPTION) {
				boolean success = ClientRequest.login(new User(loginText.getText(), passwordText.getText()));
				if (success) {
					FullSchedulePanel.this.setUserButtonsVisible(true);
					JOptionPane.showMessageDialog(null, "Login successful!");
					logger.info("Logged in as: " + loginText);
				}
				else {
					JOptionPane.showMessageDialog(null, "Wrong login/password combination!");
					logger.warn("Wrong password for user " + loginText);
				}
			}
		}
	}
}
