package com.javaschool.railwayticket;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;

@Embeddable
public class SchedTemplateLinesPK implements Serializable {
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "TemplateId")
	private SchedTemplate schedTemplate;
	@Column
	@Min(1)
	private int pointNum;
	
	public SchedTemplateLinesPK() {
		
	}
	public SchedTemplateLinesPK(SchedTemplate st, int pointNum) {
		this.schedTemplate = st;
		this.pointNum = pointNum; 
	}
	
	public SchedTemplate getSchedTemplate() {
		return schedTemplate;
	}
	public void setSchedTemplate(SchedTemplate schedTemplate) {
		this.schedTemplate = schedTemplate;
	}
	public int getPointNum() {
		return pointNum;
	}
	public void setPointNum(int pointNum) {
		this.pointNum = pointNum;
	}
	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (o instanceof SchedTemplateLinesPK) {
			SchedTemplateLinesPK stlPk = (SchedTemplateLinesPK)o;
			return stlPk.getSchedTemplate().getId() == this.getSchedTemplate().getId()
				&& stlPk.getPointNum() == this.getPointNum();
		}
		else {
			return false;
		}
	}
	@Override
	public int hashCode() {
		return (int)getSchedTemplate().getId() * 37 + pointNum;
	}
	
}
