package com.javaschool.railwayticket;

import java.io.Serializable;

public class Response implements Serializable {
	private static final long serialVersionUID = -2304215866418114219L;
	public boolean error;
	public String message;
	public Serializable obj;
	public Response(Serializable obj) {
		error = false;
		message = "";
		this.obj = obj;
	}
	public Response(boolean error, String message, Serializable obj) {
		this.error = error;
		this.message = message;
		this.obj = obj;
	}
	@Override
	public String toString() {
		return "[error: " + error + ", message: " + message + ", obj: " + obj + "]"; 
	}
}
