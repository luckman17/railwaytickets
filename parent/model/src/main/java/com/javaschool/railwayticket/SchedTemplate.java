package com.javaschool.railwayticket;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class SchedTemplate implements Serializable {
	private static final long serialVersionUID = 3L;
	@Id
	@GeneratedValue
	private long id;
	@Column
	@NotNull(message = "Schedule template name should be non-empty")
	private String templateName;

	@OneToMany (mappedBy = "key.schedTemplate", fetch = FetchType.EAGER)
	private List<SchedTemplateLines> schedTemplateLines;
	
	public List<SchedTemplateLines> getSchedTemplateLines() {
		return schedTemplateLines;
	}
	public void setSchedTemplateLines(List<SchedTemplateLines> schedTemplateLines) {
		this.schedTemplateLines = schedTemplateLines;
	}
	
	public long getId() {
		return id;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String toString() {
		return id + ". " + templateName; 
	}
	
}
