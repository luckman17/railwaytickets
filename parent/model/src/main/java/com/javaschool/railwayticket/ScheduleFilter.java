package com.javaschool.railwayticket;

import java.io.Serializable;
import java.util.Date;

public class ScheduleFilter implements Serializable {
	private static final long serialVersionUID = 345441016991304225L;
	public Date dateStart;
	public long stationIdFrom;
	public long stationIdTo;
	@Override
	public String toString() {
		String ret;
		if (dateStart == null)
			ret = "null";
		else
			ret = Global.dateFormat.format(dateStart);
		ret = "[dateStart: " + ret;
		return ret +
				", stationFrom: " + stationIdFrom +
				", stationTo: " + stationIdTo + "]";
	}
}
