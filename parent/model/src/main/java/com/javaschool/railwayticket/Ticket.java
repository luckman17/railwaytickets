package com.javaschool.railwayticket;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Ticket implements Serializable{
	private static final long serialVersionUID = 2L;
	@Id
	@GeneratedValue
	private long id;
	@ManyToOne
	@JoinColumn(name = "ScheduleId")
	private Schedule schedule;
	@ManyToOne
	@JoinColumn(name = "PassengerId")
	private Passenger passenger;
	
	public long getId() {
		return id;
	}
	public Schedule getSchedule() {
		return schedule;
	}
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	public Passenger getPassenger() {
		return passenger;
	}
	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}	
}
