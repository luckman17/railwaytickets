package com.javaschool.railwayticket;

import java.io.Serializable;

public class Request implements Serializable {
	private static final long serialVersionUID = -4575791987197351569L;
	public String text;
	public Serializable obj;
	public Request(String text) {
		this.text = text;
		obj = null;
	}
	public Request(String text, Serializable obj) {
		this.text = text;
		this.obj = obj;
	}
	@Override
	public String toString() {
		String ret;
		if (text == null)
			ret = "[text: null";
		else 
			ret = "[text: " + text;
		ret += ", obj: " + obj + "]";
		return ret;
	}
}
