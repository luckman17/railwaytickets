package com.javaschool.railwayticket;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalTime;

@Entity
@Table(uniqueConstraints = {
		@UniqueConstraint(columnNames = {"TemplateId", "dateStart"})
})
public class Schedule implements Serializable {
	private static final long serialVersionUID = -222742999167295180L;
	@Id
	@GeneratedValue
	private long id;
	@ManyToOne
	@JoinColumn(name = "TrainNum")
	@NotNull(message = "Train number is mandatory field")
	private Train train;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TemplateId")
	private SchedTemplate schedTemplate;
	@Column
	@Temporal(TemporalType.DATE)
	@NotNull(message = "Start date is mandatory field")
	private Date dateStart;
	
	@OneToMany(mappedBy="schedule", fetch = FetchType.EAGER)
	private Set<Ticket> tickets;
	
	public Set<Ticket> getTickets() {
		return tickets;
	}
	public void setTickets(Set<Ticket> tickets) {
		this.tickets = tickets;
	}
	public long getId() {
		return id;
	}
	public Train getTrain() {
		return train;
	}
	public void setTrain(Train train) {
		this.train = train;
	}
	public SchedTemplate getSchedTemplate() {
		return schedTemplate;
	}
	public void setSchedTemplate(SchedTemplate schedTemplate) {
		this.schedTemplate = schedTemplate;
	}
	public Date getDateStart() {
		return dateStart;
	}
	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}
	
	// TODO add comment
	public Date getStationDateTime(Station s) {
		long stationId = 0;
		if (s != null)
			stationId = s.getId();
		DateTime startDt = new DateTime(dateStart);
		Date lastTime = null;
		for (SchedTemplateLines stl : schedTemplate.getSchedTemplateLines()) {
			if (lastTime != null) {
				long delta = stl.getTimePass().getTime() - lastTime.getTime();
				if (delta < 0)
					delta += 24 * 3600 * 1000;
				startDt = startDt.plus(new Duration(delta));
			}
			else {
				LocalTime departTime = new LocalTime(stl.getTimePass());
				startDt = startDt.plus(departTime.getMillisOfDay());
			}
			lastTime = stl.getTimePass();
			if (stl.getStation().getId() == stationId)
				break;
		}
		return startDt.toDate();
	}
	
	public Date getFinalStationDateTime() {
		return this.getStationDateTime(null);
	}
	
	public int getEmptySeats() {
		int seats = train.getSeats();
		int ticketsSell = tickets.size();
		return seats - ticketsSell;
	}
}
