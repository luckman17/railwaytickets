package com.javaschool.railwayticket;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
public class SchedTemplateLines implements Serializable {
	private static final long serialVersionUID = 1L;	
	@EmbeddedId
	private SchedTemplateLinesPK key;
	
	@ManyToOne
	@JoinColumn(name = "StationId")
	private Station station;
	@Column
	@Temporal(TemporalType.TIME)
	@NotNull (message = "Please, enter time")
	private Date timePass;
	
	public Station getStation() {
		return station;
	}
	public void setStation(Station station) {
		this.station = station;
	}
	public Date getTimePass() {
		return timePass;
	}
	public void setTimePass(Date timePass) {
		this.timePass = timePass;
	}
	public SchedTemplateLinesPK getKey() {
		return key;
	}
	public void setKey(SchedTemplateLinesPK key) {
		this.key = key;
	}
	
	public String toString() {
		return "TemplateId# " + key.getSchedTemplate().getId() + " " + 
				"PointNum " + key.getPointNum() + " " + 
				"Station " + station.getId() + "(" + station.getName() + ")" +
				"TimePass " + timePass;
				
	}
	
	
}
