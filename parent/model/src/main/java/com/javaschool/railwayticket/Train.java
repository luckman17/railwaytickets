package com.javaschool.railwayticket;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class Train implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private long number;
	@Column
	@NotNull
	@Min(1)
	private int seats;
	
	public long getNumber() {
		return number;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	
	@Override
	public String toString() {
		return "Train #" + number + " with " + seats + " seats.";
	}
	
}
