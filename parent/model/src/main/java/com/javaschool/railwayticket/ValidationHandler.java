package com.javaschool.railwayticket;

import java.util.Set;

import javax.validation.ConstraintViolation;

public final class ValidationHandler {
	ValidationHandler() {}
	public static String getValidationErrors(Object obj) {
		Set<ConstraintViolation<Object> > constraintViolations = Global.validator.validate(obj);
		String ret = "";
		if (!constraintViolations.isEmpty()) {
			ret = "Validation errors:\n";
		}
		for (ConstraintViolation<Object> cv : constraintViolations) {
			ret += cv.getMessage() + "\n";
		}
		return ret;
	}
}
