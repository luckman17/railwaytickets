package com.javaschool.railwayticket;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NotFound;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Entity
public class Passenger implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private long id;
	
	@Column
	@NotNull
	@Size(min = 2, max = 50, message = "Firstname length must be from 2 to 50")
	private String firstname;
	@Column
	@NotNull
	@Size(min = 2, max = 50, message = "Lastname length must be from 2 to 50")
	private String lastname;
	@Column
	@Temporal(TemporalType.DATE)
	@Past(message = "Wrong birthdate")
	private Date birthdate;
	
	public long getId() {
		return id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	
}
