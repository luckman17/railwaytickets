package com.javaschool.railwayticket;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class TrainDao extends Dao {
	EntityManager em = Managers.em;
	public Train find(long trainNum) {
		Train ret = em.find(Train.class, trainNum);
		return ret;
	}
	public void delete(Train t) {
		em.remove(t);
	}
	public List<Train> getAllTrains() {
		List<Train> ret = em.createQuery("select t from Train t", Train.class).getResultList();
		return ret;
	}
}
