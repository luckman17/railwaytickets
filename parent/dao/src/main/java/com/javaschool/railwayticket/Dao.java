package com.javaschool.railwayticket;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public abstract class Dao {
	protected EntityManager em = Managers.em;
	public void insert(Object obj) {
		em.persist(obj);
	}
	
	public void update(Object obj) {
		this.insert(obj);
	}
}
