package com.javaschool.railwayticket;

import java.util.List;

import javax.persistence.EntityTransaction;

public class SchedTemplateDao extends Dao {
	public void insertWithLines(SchedTemplate st) {
		em.persist(st);
		if (st.getSchedTemplateLines() != null) {
			for (SchedTemplateLines stl : st.getSchedTemplateLines()) {
				stl.getKey().setSchedTemplate(st);
				new SchedTemplateLinesDao().insert(stl);
			}
		}
	}
	public List<SchedTemplate> getAllSchedTemplates() {
		List<SchedTemplate> ret = em.createQuery("select st from SchedTemplate st", SchedTemplate.class).getResultList();
		return ret;
	}
}
