package com.javaschool.railwayticket;

import java.util.List;

public class StationDao extends Dao {
	public Station findByName(String name) {
		Station ret = em.createQuery("select s from Station s where s.name = :name",
				Station.class).setParameter("name", name).getSingleResult();
		return ret;
	}
	public Station find(long id) {
		Station ret = em.find(Station.class, id);
		return ret;
	}
	public List<Station> getAllStations() {
		List<Station> ret = em.createQuery("select s from Station s", Station.class).getResultList();
		return ret;
	}
}
