package com.javaschool.railwayticket;

import java.util.List;

import javax.persistence.TypedQuery;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalTime;

public class TicketDao extends Dao {
	/**
	 * @return error String if unable to buy a ticket, empty String otherwise
	 */
	public String buyTicket(Passenger p, Schedule s) {
		String ret = "";
		p = new PassengerDao().registerPassenger(p);
		TypedQuery<Ticket> query = em.createQuery("select t from Ticket t"
				+ " where t.passenger = :passenger"
				+ " and t.schedule = :schedule", Ticket.class);
		query.setParameter("passenger", p);
		query.setParameter("schedule", s);
		List<Ticket> list = query.getResultList();		
		if (!list.isEmpty()) {
			ret = "Unable to buy ticket because this passenger already have ticket on this train";
		}
		else {		
			// check empty seats
			if (new ScheduleDao().getTicketsLeft(s) <= 0) {
				ret = "Sorry, but the train is already full"; 
			}
			else {
				// check depart time
				DateTime currentDT = new DateTime();
				DateTime departDT = new DateTime(s.getDateStart());
				LocalTime departTime = new LocalTime(s.getSchedTemplate().getSchedTemplateLines().get(0).getTimePass());
				departDT = departDT.plus(departTime.getMillisOfDay());
				Duration duration = new Duration(currentDT, departDT);
				if (duration.isShorterThan(new Duration(10 * 60 * 1000L)))
					ret = "The train leaves in less than 10 minutes";
			}
		}
		
		if (ret.equals("")) {
			Ticket t = new Ticket();
			t.setPassenger(p);
			t.setSchedule(s);
			new TicketDao().insert(t);
		}
			
		return ret;
	}
}
