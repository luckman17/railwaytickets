package com.javaschool.railwayticket;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Managers {
	public static final EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("railwayticket");
	public static final EntityManager em = emf.createEntityManager();
}
