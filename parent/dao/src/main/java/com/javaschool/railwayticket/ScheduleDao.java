package com.javaschool.railwayticket;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class ScheduleDao extends Dao {
	public Schedule find(long id) {
		Schedule ret = em.find(Schedule.class, id);
		return ret;
	}
	public void delete(Schedule s) {
		em.remove(s);
	}
	
	public List<Schedule> getFullSchedule() {
		List<Schedule> ret = em.createQuery("select s from Schedule s", Schedule.class).getResultList();		
		return ret;
	}
	public List<Schedule> getFilteredSchedule(ScheduleFilter sf) {
		em.clear();
		Date date = sf.dateStart;
		List<Schedule> schedules = null;
		if (date != null) {
			TypedQuery<Schedule> query = em.createQuery(
					"select s from Schedule s where s.dateStart = :dateStart", Schedule.class);
			schedules = query.setParameter("dateStart", date).getResultList();
		}
		else {
			schedules = em.createQuery("select s from Schedule s", Schedule.class).getResultList();
		}
		
		List<Schedule> ret = new ArrayList<Schedule>();
		
		for (Schedule s : schedules) {
			List<SchedTemplateLines> lines = s.getSchedTemplate().getSchedTemplateLines();
			boolean firstStation = false;
			boolean secondStation = false;
			if (sf.stationIdFrom == 0)
				firstStation = true;
			if (sf.stationIdTo == 0)
				secondStation = true;
			for (SchedTemplateLines l : lines) {
				if (!firstStation && l.getStation().getId() == sf.stationIdFrom)
					firstStation = true;
				if (firstStation && l.getStation().getId() == sf.stationIdTo)
					secondStation = true;
			}
			if (firstStation && secondStation) {
				s.getTickets();
				ret.add(s);
			}
		}
		return ret;
	}
	public List<Passenger> getPassengerList(Schedule s) {
		TypedQuery<Passenger> query = em.createQuery(
					"select p from Ticket t join t.passenger p"
				+ " where t.schedule = :schedule", Passenger.class);
		query.setParameter("schedule", s);
		List<Passenger> ret = query.getResultList();
		return ret;
	}
	
	public int getTicketsLeft(Schedule s) {
		Train train = s.getTrain();
		String queryString = "Select count(t) from Ticket t"
				+ " where t.schedule = :schedule";
		Query queryCount = em.createQuery(queryString);
		queryCount.setParameter("schedule", s);
		int ticketsSell = ((Long) queryCount.getSingleResult()).intValue();
		return train.getSeats() - ticketsSell;
	}
}
