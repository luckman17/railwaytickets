package com.javaschool.railwayticket;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class PassengerDao extends Dao {
	
	public Passenger findByNameDate(String firstName, String lastName, Date birthdate) {
		TypedQuery<Passenger> q = em.createQuery("select p from Passenger p"
				+ " where p.firstname = :firstname"
				+ " and p.lastname = :lastname"
				+ " and p.birthdate = :birthdate", Passenger.class);
		q.setParameter("firstname", firstName);
		q.setParameter("lastname", lastName);
		q.setParameter("birthdate", birthdate);
		List<Passenger> list = q.getResultList();
		Passenger ret = null;
		if (list.size() == 1)
			ret = list.get(0);
		else if (list.size() >= 2) {
			// TODO Error
		}
		return ret;
	}
	
	public Passenger registerPassenger(Passenger p) {
		Passenger passenger = this.findByNameDate(p.getFirstname(), p.getLastname(), p.getBirthdate());
		if (passenger == null) {
			passenger = p;
			insert(passenger);
		}
		return passenger;
	}
}
