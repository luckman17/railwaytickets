package com.javaschool.railwayticket;

public class UserDao extends Dao {
	public boolean login(User u) {
		User user = em.find(User.class, u.getLogin());
		if (user == null) {
			return false;
		}
		else {
			return user.getPassword().equals(u.getPassword());
		}
	}
}
