package com.javaschool.railwayticket;

import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;

public class SingleSocketHandler implements Runnable {
	private Socket socket;
	private final Logger logger = Logger.getLogger(SingleSocketHandler.class);
	
	public SingleSocketHandler(Socket socket) {
		this.socket = socket;
	}
	
	public void run() {
		try {
			ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
			Request req = (Request)input.readObject();
			
			// send response
			Response obj = this.handleRequest(req);
			ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
			output.writeObject(obj);
			output.flush();
			socket.shutdownOutput();
			socket.close();
		}
		catch (IOException|ClassNotFoundException exception) {
			logger.error("Error", exception);
		}
		catch (Exception e) {
			if (!socket.isOutputShutdown()) {	// will send error message to client
				try {
					Response obj = new Response(true, "", e);
					ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
					output.writeObject(obj);
					output.flush();
				} catch (IOException e1) {
					logger.error("Error during sending error message to client", e1);
				}
			}
			logger.error("", e);			
		}
		finally {
			try {
				if (socket != null && !socket.isClosed()) {
					socket.close();
				}
			}
			catch (Exception e) {
				logger.error("Error", e); // TODO exception
			}
		}
	}
	
	private Response handleRequest(Request req) {
		Response ret = null;
		logger.info("Recieved request " + req);
		EntityTransaction trans = Managers.em.getTransaction();
		
		try {
			trans.begin();
			if (req.text.equals("Ping"))
				ret = new Response("Pong");
			else if (req.text.equals("Random Train")) {
				List<Train> trains = new TrainDao().getAllTrains();
				Random rand = new Random();
				ret = new Response(trains.get(rand.nextInt(trains.size())));
			}
			else if (req.text.equals("Get all trains")) {
				ret = new Response((Serializable)(new TrainDao().getAllTrains()));
			}
			else if (req.text.equals("Get full schedule")) {
				ret = new Response((Serializable)(new ScheduleDao().getFullSchedule()));
			}
			else if (req.text.equals("Get filtered schedule")) {
				ret = new Response((Serializable)
						(new ScheduleDao().getFilteredSchedule((ScheduleFilter)req.obj)));
			}
			else if (req.text.equals("Get station by id")) {
				ret = new Response(new StationDao().find((Long)(req.obj)));
			}
			else if (req.text.equals("Buy ticket")) {
				Tuple<Long, Passenger> tuple = (Tuple<Long, Passenger>)req.obj;
				Schedule schedule = new ScheduleDao().find(tuple.first);
				String result = new TicketDao().buyTicket(tuple.second, schedule);
				if (result == "") {
					ret = new Response(false,
							"Ticket to route: " + schedule.getSchedTemplate().getTemplateName() + "\n" +
							"Train number: " + schedule.getTrain().getNumber() + "\n" +
							"Departure date: " + Global.dateFormat.format(schedule.getDateStart()),
							null);
				}
				else {
					ret = new Response(true, result, null);				
				}
			}
			else if (req.text.equals("Add train")) {
				new TrainDao().insert(req.obj);
				ret = new Response(null);
			}
			else if (req.text.equals("Add station")) {
				new StationDao().insert(req.obj);
				ret = new Response(null);
			}
			else if (req.text.equals("Get schedule passenger list")) {
				List<Passenger> list = new ScheduleDao().getPassengerList((Schedule)req.obj);
				ret = new Response((Serializable)list);
			}
			else if (req.text.equals("Get all stations")) {
				List<Station> list = new StationDao().getAllStations();
				ret = new Response((Serializable)list);
			}
			else if (req.text.equals("Add schedTemplate")) {
				new SchedTemplateDao().insertWithLines((SchedTemplate)req.obj);
				ret = new Response(null);
			}
			else if (req.text.equals("Get all schedTemplates")) {
				List<SchedTemplate> list = new SchedTemplateDao().getAllSchedTemplates();
				ret = new Response((Serializable)list);
			}
			else if (req.text.equals("Add schedule")) {
				try {
					new ScheduleDao().insert(req.obj);
					ret = new Response(null);
				}
				catch (Exception e) {
					ret = new Response(true, "Couldn't add this schedule", null);
				}
			}
			else if (req.text.equals("Login")) {
				boolean success = new UserDao().login((User)req.obj);
				ret = new Response(!success, "", null);			
			}
			else if (req.text.equals("Get tickets left")) {
				int ticketsLeft = new ScheduleDao().getTicketsLeft((Schedule)req.obj);
				ret = new Response(false, "", ticketsLeft);
			}
			else {
				ret = new Response(true, "\"" + req.text + "\"" + " is wrong request", null);
				logger.error("Wrong request: " + req.toString());
			}
			trans.commit();
			logger.info("Send response " + ret.toString());
		} catch (Exception e) {
			if (trans.isActive()) {
				trans.rollback();
			}
			if (ret == null) {
				ret = new Response(true, "", e);
			}
		}
		return ret;
	}
	
}
