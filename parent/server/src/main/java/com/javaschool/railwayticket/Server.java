package com.javaschool.railwayticket;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.*;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.persistence.EntityManagerFactory;

//import javax.persistence.EntityManagerFactory;

public class Server 
{
	private boolean running = false;
	private ServerSocket serverSocket;
	private final Logger logger = Logger.getLogger(Server.class);
	private final ExecutorService pool = Executors.newFixedThreadPool(3);	
	
	public void startServer() {
		if (!running) {
			EntityManagerFactory emf = Managers.emf;
			Socket socket = null;
			try {
				serverSocket = new ServerSocket(8080);
				logger.info("Server started");
				running = true;				
				while (true) {
					socket = serverSocket.accept();
					pool.execute(new SingleSocketHandler(socket));
				}
			}
			catch (IOException ioe) {
				logger.error("Cannot start server", ioe);
			}
			finally {
				try {
					if (socket != null && !socket.isClosed())
						socket.close();
					if (serverSocket != null && !serverSocket.isClosed())
						serverSocket.close();
				}
				catch (Exception ex){
					logger.error("Error in finally", ex);
				}
			}
		}
	}	
	
	public void shutDownServer() {
		try {
			serverSocket.close();
		}
		catch (IOException ioe) {
			logger.error("", ioe);
		}
		logger.info("Shutdown server");
		running = false;
	}
	
    public static void main( String[] args )
    {
    	Server server = new Server();
    	ServerFrame.startFrame(server);
    }
}
