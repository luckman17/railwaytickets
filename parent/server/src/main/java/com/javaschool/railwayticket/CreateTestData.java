package com.javaschool.railwayticket;

import java.text.SimpleDateFormat;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class CreateTestData {
	public static void main(String[] args) throws Exception {
		EntityManager em = Managers.em;
		
		// delete data
		EntityTransaction trans = em.getTransaction();
		try {
			trans.begin();
			em.createQuery("delete from Ticket t").executeUpdate();
			em.createQuery("delete from SchedTemplateLines s").executeUpdate();
			em.createQuery("delete from Schedule s").executeUpdate();
			em.createQuery("delete from Station s").executeUpdate();
			em.createQuery("delete from SchedTemplate s").executeUpdate();
			em.createQuery("delete from Train t").executeUpdate();
			em.createQuery("delete from Passenger p").executeUpdate();
			trans.commit();
		}
		catch (Exception e) {
			if (trans.isActive()) {
				trans.rollback();
				e.printStackTrace();
				return;
			}
		}
		
		trans.begin();
		
		// add Trains
		TrainDao trainDao = new TrainDao();
		Train t1 = new Train();
		t1.setSeats(5);
		trainDao.insert(t1);
		Train t2 = new Train();
		t2.setSeats(4);
		trainDao.insert(t2);
		Train t3 = new Train();
		t3.setSeats(7);
		trainDao.insert(t3);
		
		// add Stations
		StationDao stationDao = new StationDao();
		Station s;
		for (int i = 0; i < 8; i++) {
			s = new Station();
			s.setName("Station " + (char)('A' + i));
			stationDao.insert(s);
		}

		// add SchedTemplates
		SchedTemplateDao stDao = new SchedTemplateDao();
		SchedTemplateLinesDao stlDao = new SchedTemplateLinesDao();
		SchedTemplateLines stl;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		
		SchedTemplate template1 = new SchedTemplate();
		template1.setTemplateName("Daily Route A-D");
		stDao.insert(template1);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template1, 1));
		stl.setStation(stationDao.findByName("Station A"));
		stl.setTimePass(sdf.parse("08:10"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template1, 2));
		stl.setStation(stationDao.findByName("Station B"));
		stl.setTimePass(sdf.parse("09:08"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template1, 3));
		stl.setStation(stationDao.findByName("Station C"));
		stl.setTimePass(sdf.parse("11:02"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template1, 4));
		stl.setStation(stationDao.findByName("Station D"));
		stl.setTimePass(sdf.parse("11:39"));
		stlDao.insert(stl);
		
		SchedTemplate template2 = new SchedTemplate();
		template2.setTemplateName("Route D-G #1");
		stDao.insert(template2);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template2, 1));
		stl.setStation(stationDao.findByName("Station D"));
		stl.setTimePass(sdf.parse("09:44"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template2, 2));
		stl.setStation(stationDao.findByName("Station E"));
		stl.setTimePass(sdf.parse("10:50"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template2, 3));
		stl.setStation(stationDao.findByName("Station F"));
		stl.setTimePass(sdf.parse("12:22"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template2, 4));
		stl.setStation(stationDao.findByName("Station G"));
		stl.setTimePass(sdf.parse("13:28"));
		stlDao.insert(stl);
		
		SchedTemplate template3 = new SchedTemplate();
		template3.setTemplateName("Route G-H");
		stDao.insert(template3);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template3, 1));
		stl.setStation(stationDao.findByName("Station G"));
		stl.setTimePass(sdf.parse("10:11"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template3, 2));
		stl.setStation(stationDao.findByName("Station F"));
		stl.setTimePass(sdf.parse("11:05"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template3, 3));
		stl.setStation(stationDao.findByName("Station E"));
		stl.setTimePass(sdf.parse("12:37"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template3, 4));
		stl.setStation(stationDao.findByName("Station C"));
		stl.setTimePass(sdf.parse("13:02"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template3, 5));
		stl.setStation(stationDao.findByName("Station H"));
		stl.setTimePass(sdf.parse("15:00"));
		stlDao.insert(stl);
		
		SchedTemplate template4 = new SchedTemplate();
		template4.setTemplateName("Route D-G #2");
		stDao.insert(template4);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template4, 1));
		stl.setStation(stationDao.findByName("Station D"));
		stl.setTimePass(sdf.parse("23:36"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template4, 2));
		stl.setStation(stationDao.findByName("Station E"));
		stl.setTimePass(sdf.parse("00:39"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template4, 3));
		stl.setStation(stationDao.findByName("Station F"));
		stl.setTimePass(sdf.parse("02:08"));
		stlDao.insert(stl);
		stl = new SchedTemplateLines();
		stl.setKey(new SchedTemplateLinesPK(template4, 4));
		stl.setStation(stationDao.findByName("Station G"));
		stl.setTimePass(sdf.parse("03:06"));
		stlDao.insert(stl);
		
		
		// add schedule
		Schedule schedule;
		SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yyyy");
		
		schedule = new Schedule();
		schedule.setTrain(t1);
		schedule.setSchedTemplate(template1);
		schedule.setDateStart(formatDate.parse("02.04.2014"));
		new ScheduleDao().insert(schedule);
		
		schedule = new Schedule();
		schedule.setTrain(t1);
		schedule.setSchedTemplate(template1);
		schedule.setDateStart(formatDate.parse("03.04.2014"));
		new ScheduleDao().insert(schedule);

		schedule = new Schedule();
		schedule.setTrain(t1);
		schedule.setSchedTemplate(template1);
		schedule.setDateStart(formatDate.parse("04.04.2014"));
		new ScheduleDao().insert(schedule);
		
		schedule = new Schedule();
		schedule.setTrain(t1);
		schedule.setSchedTemplate(template1);
		schedule.setDateStart(formatDate.parse("05.04.2014"));
		new ScheduleDao().insert(schedule);

		schedule = new Schedule();
		schedule.setTrain(t1);
		schedule.setSchedTemplate(template1);
		schedule.setDateStart(formatDate.parse("06.04.2014"));
		new ScheduleDao().insert(schedule);		
		
		schedule = new Schedule();
		schedule.setTrain(t2);
		schedule.setSchedTemplate(template2);
		schedule.setDateStart(formatDate.parse("02.04.2014"));
		new ScheduleDao().insert(schedule);

		schedule = new Schedule();
		schedule.setTrain(t2);
		schedule.setSchedTemplate(template2);
		schedule.setDateStart(formatDate.parse("04.04.2014"));
		new ScheduleDao().insert(schedule);
		
		schedule = new Schedule();
		schedule.setTrain(t2);
		schedule.setSchedTemplate(template2);
		schedule.setDateStart(formatDate.parse("06.04.2014"));
		new ScheduleDao().insert(schedule);
		
		
		schedule = new Schedule();
		schedule.setTrain(t2);
		schedule.setSchedTemplate(template3);
		schedule.setDateStart(formatDate.parse("03.04.2014"));
		new ScheduleDao().insert(schedule);

		schedule = new Schedule();
		schedule.setTrain(t2);
		schedule.setSchedTemplate(template3);
		schedule.setDateStart(formatDate.parse("05.04.2014"));
		new ScheduleDao().insert(schedule);
		
		schedule = new Schedule();
		schedule.setTrain(t3);
		schedule.setSchedTemplate(template4);
		schedule.setDateStart(formatDate.parse("02.04.2014"));
		new ScheduleDao().insert(schedule);
		
		schedule = new Schedule();
		schedule.setTrain(t3);
		schedule.setSchedTemplate(template4);
		schedule.setDateStart(formatDate.parse("03.04.2014"));
		new ScheduleDao().insert(schedule);

		schedule = new Schedule();
		schedule.setTrain(t3);
		schedule.setSchedTemplate(template4);
		schedule.setDateStart(formatDate.parse("05.04.2014"));
		new ScheduleDao().insert(schedule);
		
		trans.commit();
		
	}
}
