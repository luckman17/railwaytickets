package com.javaschool.railwayticket;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ServerFrame extends JFrame {
	private Server server;
	private JLabel label = new JLabel("Port: 8080");
	private JButton startButton;
	private JButton endButton;	
	
	public ServerFrame(Server server) {		
		super("Railway Tickets server");
		this.server = server;
		setSize(200, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		add(label);
		
		this.addButtonsPanel();		
		setVisible(true);
	}
	
	public static void startFrame(final Server server) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new ServerFrame(server);
			}
		});
	}
	
	private void addButtonsPanel() {
		GridLayout gridLayout = new GridLayout(2, 1);
		gridLayout.setVgap(5);
		JPanel buttonPanel = new JPanel(gridLayout);
		
		startButton = new JButton("Start");
		endButton = new JButton("Shutdown");
		buttonPanel.add(startButton);
		buttonPanel.add(endButton);
		
		JPanel east = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.weighty = 1;
		east.add(buttonPanel, gbc);
		
		add(east, BorderLayout.EAST);
		
		startButton.addActionListener(new StartActionListener());
		endButton.addActionListener(new EndActionListener());
	}
	
	class StartActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Thread t = new Thread() {
				public void run() {
					server.startServer();
				}
			};
			t.start();
		}
	}
	
	class EndActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			server.shutDownServer();
		}
	}
	
}
